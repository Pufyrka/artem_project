#[[1, 1, 1], [1, 1, 0], [1, 0, 1]]
length = int(input('Введи длину'))
sr = int(input('Введи строку'))
sc = int(input('Введи столбец'))
newColor = int(input('Новый цвет'))

image = []
for i in range(length):
    row = input().split()
    for i in range(len(row)):
        row[i] = int(row[i])
    image.append(row)

oldColor = image[sr][sc]


def fill(row, column):
    global image
    global sr
    global sc
    global newColor
    global oldColor
    global length

    image[row][column] = newColor

    if (row > 0) and (row < length-1):
        if image[row-1][column] == oldColor:
            fill(row-1, column)
        if image[row+1][column] == oldColor:
            fill(row+1, column)
    if (column > 0) and (column < length-1):
        if image[row][column-1] == oldColor:
            fill(row, column-1)
        if image[row][column+1] == oldColor:
            fill(row, column+1)


fill(sr, sc)
print(image)

